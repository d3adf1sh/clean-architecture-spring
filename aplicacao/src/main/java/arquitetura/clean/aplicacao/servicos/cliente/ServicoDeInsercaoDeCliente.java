package arquitetura.clean.aplicacao.servicos.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteJaCadastrado;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import arquitetura.clean.modelo.entidades.cliente.Cliente;

import java.util.Objects;
import java.util.Optional;

public final class ServicoDeInsercaoDeCliente implements CasoDeUsoDeInsercaoDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeInsercaoDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public ClienteDeResposta inserir(ClienteDeRequisicao clienteDeRequisicao) throws ClienteJaCadastrado {
        Objects.requireNonNull(clienteDeRequisicao, "O argumento \"clienteDeRequisicao\" não pode ser nulo.");
        Cliente cliente = ClienteDeRequisicao.converterParaModelo(clienteDeRequisicao);
        Optional<Cliente> clienteExistente = repositorioDeClientes.buscar(cliente.getCpfj());
        if (clienteExistente.isPresent()) {
            throw new ClienteJaCadastrado("Cliente %s já cadastrado.".formatted(cliente.getCpfj().formatar()));
        }

        return ClienteDeResposta.converterParaResposta(repositorioDeClientes.inserir(cliente));
    }
}
