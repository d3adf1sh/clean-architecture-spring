package arquitetura.clean.aplicacao.modelo.requisicao.cliente;

import arquitetura.clean.modelo.entidades.cliente.Telefone;
import arquitetura.clean.modelo.valores.Numero;

public record TelefoneDeRequisicao(String descricao, String numero) {
    public static Telefone converterParaModelo(TelefoneDeRequisicao telefoneDeRequisicao) {
        Numero numero = telefoneDeRequisicao.numero() != null ? Numero.de(telefoneDeRequisicao.numero()) : null;
        return new Telefone(telefoneDeRequisicao.descricao(), numero);
    }
}
