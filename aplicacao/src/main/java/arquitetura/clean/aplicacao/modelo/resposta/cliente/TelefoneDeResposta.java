package arquitetura.clean.aplicacao.modelo.resposta.cliente;

import arquitetura.clean.modelo.entidades.cliente.Telefone;

public record TelefoneDeResposta(String descricao, String numero) {
    public static TelefoneDeResposta converterParaResposta(Telefone telefone) {
        String numero = telefone.getNumero() != null ? telefone.getNumero().formatar() : null;
        return new TelefoneDeResposta(telefone.getDescricao(), numero);
    }
}
