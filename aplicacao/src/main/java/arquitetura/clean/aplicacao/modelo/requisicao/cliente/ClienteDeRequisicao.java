package arquitetura.clean.aplicacao.modelo.requisicao.cliente;

import arquitetura.clean.modelo.entidades.cliente.Cliente;
import arquitetura.clean.modelo.entidades.cliente.Contato;
import arquitetura.clean.modelo.valores.CPFJ;

import java.util.List;

public record ClienteDeRequisicao(String nomeFantasia, String razaoSocial, String cpfj,
        List<ContatoDeRequisicao> contatos) {
    public static Cliente converterParaModelo(ClienteDeRequisicao clienteDeRequisicao) {
        CPFJ cpfj = clienteDeRequisicao.cpfj() != null ? CPFJ.de(clienteDeRequisicao.cpfj()) : null;
        List<Contato> contatos = clienteDeRequisicao.contatos() != null
                ? clienteDeRequisicao.contatos().stream().map(ContatoDeRequisicao::converterParaModelo).toList() : null;
        return new Cliente(clienteDeRequisicao.nomeFantasia(), clienteDeRequisicao.razaoSocial(), cpfj, contatos);
    }
}
