package arquitetura.clean.aplicacao.casosdeuso.cliente;

public class ClienteJaCadastrado extends Exception {
    public ClienteJaCadastrado() {
    }

    public ClienteJaCadastrado(String message) {
        super(message);
    }

    public ClienteJaCadastrado(String message, Throwable cause) {
        super(message, cause);
    }

    public ClienteJaCadastrado(Throwable cause) {
        super(cause);
    }

    public ClienteJaCadastrado(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
