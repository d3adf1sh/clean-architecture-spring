package arquitetura.clean.aplicacao.casosdeuso.cliente;

import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;

public interface CasoDeUsoDeAlteracaoDeCliente {
    ClienteDeResposta alterar(ClienteDeRequisicao clienteDeRequisicao) throws ClienteNaoEncontrado;
}
