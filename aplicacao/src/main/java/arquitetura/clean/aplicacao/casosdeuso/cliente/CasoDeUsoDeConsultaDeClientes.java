package arquitetura.clean.aplicacao.casosdeuso.cliente;

import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;

import java.util.List;

public interface CasoDeUsoDeConsultaDeClientes {
    List<ClienteDeResposta> consultar(String nomeFantasia);
}
