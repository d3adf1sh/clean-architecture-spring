package arquitetura.clean.bootstrap;

import arquitetura.clean.adaptadores.Adaptadores;
import org.springframework.boot.SpringApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Properties;

public final class Inicializador {
    public static void main(String[] args) {
        Inicializador inicializador = new Inicializador();
        inicializador.carregarPropriedades();
        inicializador.configurarAdaptadoresDePersistencia();
        inicializador.iniciarServidor(args);
    }

    private void carregarPropriedades() {
        String nomeDoArquivo = System.getProperty("arquivoDePropriedades");
        if (nomeDoArquivo == null) {
            File jar = new File(Inicializador.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            nomeDoArquivo = Path.of(jar.getParent(), "config.properties").toString();
        }

        carregarPropriedades(nomeDoArquivo);
    }

    private void carregarPropriedades(String nomeDoArquivo) {
        File arquivo = new File(nomeDoArquivo);
        if (arquivo.exists() && arquivo.isFile()) {
            try (InputStream streamDoArquivo = new FileInputStream(arquivo)) {
                Properties propriedades = new Properties();
                propriedades.load(streamDoArquivo);
                for (String nome : propriedades.stringPropertyNames()) {
                    String valor = propriedades.getProperty(nome);
                    if (valor != null && !valor.isEmpty()) {
                        System.setProperty(nome, valor);
                    }
                }
            } catch (IOException failure) {
                failure.printStackTrace();
            }
        }
    }

    private void configurarAdaptadoresDePersistencia() {
        String persistencia = lerPropriedade("persistencia", "");
        switch (persistencia) {
            case "mysql" -> configurarAdaptadoresParaMySQL();
            case "postgres" -> configurarAdaptadoresParaPostgres();
            case "mongodb" -> configurarAdaptadoresParaMongoDB();
            default -> throw new IllegalArgumentException("Persistência \"%s\" inválida.".formatted(persistencia));
        }
    }

    private void configurarAdaptadoresParaMySQL() {
        configurarAdaptadoresParaJPA(
                "jdbc:mysql://%s:%d/%s".formatted(lerPropriedade("persistencia.mysql.servidor", "localhost"),
                        Integer.parseInt(lerPropriedade("persistencia.mysql.porta", "3306")),
                        lerPropriedade("persistencia.mysql.bancoDeDados")),
                lerPropriedade("persistencia.mysql.usuario"),
                lerPropriedade("persistencia.mysql.senha"));
    }

    private void configurarAdaptadoresParaPostgres() {
        configurarAdaptadoresParaJPA(
                "jdbc:postgresql://%s:%d/%s".formatted(lerPropriedade("persistencia.postgres.servidor", "localhost"),
                        Integer.parseInt(lerPropriedade("persistencia.postgres.porta", "5432")),
                        lerPropriedade("persistencia.postgres.bancoDeDados")),
                lerPropriedade("persistencia.postgres.usuario"),
                lerPropriedade("persistencia.postgres.senha"));
    }

    private void configurarAdaptadoresParaJPA(String url, String usuario, String senha) {
        System.setProperty("spring.datasource.url", url);
        System.setProperty("spring.datasource.username", usuario);
        System.setProperty("spring.datasource.password", senha);
        System.setProperty("spring.jpa.show-sql", "true");
        System.setProperty("spring.jpa.properties.hibernate.format_sql", "true");
        System.setProperty("spring.jpa.hibernate.ddl-auto", "update");
    }

    private void configurarAdaptadoresParaMongoDB() {
        String nomeDoBancoDeDados = lerPropriedade("persistencia.mongodb.bancoDeDados");
        System.setProperty("spring.data.mongodb.uri",
                "mongodb://%s:%s@%s:%d/?authSource=%s".formatted(lerPropriedade("persistencia.mongodb.usuario"),
                        lerPropriedade("persistencia.mongodb.senha"),
                        lerPropriedade("persistencia.mongodb.servidor", "localhost"),
                        Integer.parseInt(lerPropriedade("persistencia.mongodb.porta", "27017")),
                        nomeDoBancoDeDados));
        System.setProperty("spring.data.mongodb.database", nomeDoBancoDeDados);
    }

    private String lerPropriedade(String nome) {
        return lerPropriedade(nome, null);
    }

    private String lerPropriedade(String nome, String padrao) {
        String valor = System.getProperty(nome);
        if (valor == null || valor.isEmpty()) {
            if (padrao == null) {
                throw new IllegalArgumentException("Propriedade \"%s\" não configurada.".formatted(nome));
            }

            valor = padrao;
        }

        return valor;
    }

    private void iniciarServidor(String[] args) {
        SpringApplication.run(Adaptadores.class, args);
    }
}