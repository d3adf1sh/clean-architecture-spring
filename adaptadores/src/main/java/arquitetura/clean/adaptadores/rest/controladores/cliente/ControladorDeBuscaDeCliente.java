package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.adaptadores.rest.conversores.ConversorDeCPFJ;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeBuscaDeCliente {
    private final CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente;

    public ControladorDeBuscaDeCliente(CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente) {
        this.casoDeUsoDeBuscaDeCliente = casoDeUsoDeBuscaDeCliente;
    }

    @GetMapping("/{cpfj}")
    public ClienteDeResposta buscar(@PathVariable("cpfj") String cpfj) {
        try {
            return casoDeUsoDeBuscaDeCliente.buscar(ConversorDeCPFJ.de(cpfj));
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(HttpStatus.NOT_FOUND, failure.getMessage());
        }
    }
}