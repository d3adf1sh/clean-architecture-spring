package arquitetura.clean.adaptadores.rest.resposta;

public record RespostaWeb(int codigo, String mensagem) {
}
