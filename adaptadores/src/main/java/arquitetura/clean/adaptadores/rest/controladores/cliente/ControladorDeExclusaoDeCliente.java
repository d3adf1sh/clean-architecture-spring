package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.adaptadores.rest.conversores.ConversorDeCPFJ;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeExclusaoDeCliente {
    private final CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente;

    public ControladorDeExclusaoDeCliente(CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente) {
        this.casoDeUsoDeExclusaoDeCliente = casoDeUsoDeExclusaoDeCliente;
    }

    @DeleteMapping("/{cpfj}")
    public ClienteDeResposta excluir(@PathVariable("cpfj") String cpfj) {
        try {
            return casoDeUsoDeExclusaoDeCliente.excluir(ConversorDeCPFJ.de(cpfj));
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(HttpStatus.NOT_FOUND, failure.getMessage());
        }
    }
}

