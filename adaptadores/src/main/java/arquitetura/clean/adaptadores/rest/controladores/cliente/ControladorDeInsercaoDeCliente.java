package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteJaCadastrado;
import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;
import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarResposta;

@RestController
@RequestMapping("/cliente")
public class ControladorDeInsercaoDeCliente {
    private final CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente;

    public ControladorDeInsercaoDeCliente(CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente) {
        this.casoDeUsoDeInsercaoDeCliente = casoDeUsoDeInsercaoDeCliente;
    }

    @PostMapping
    public ResponseEntity<ClienteDeResposta> inserir(@RequestBody ClienteDeRequisicao clienteDeRequisicao) {
        try {
            return gerarResposta(HttpStatus.CREATED, casoDeUsoDeInsercaoDeCliente.inserir(clienteDeRequisicao));
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(HttpStatus.BAD_REQUEST, failure.getMessage());
        } catch (ClienteJaCadastrado failure) {
            throw gerarErro(HttpStatus.CONFLICT, failure.getMessage());
        }
    }
}