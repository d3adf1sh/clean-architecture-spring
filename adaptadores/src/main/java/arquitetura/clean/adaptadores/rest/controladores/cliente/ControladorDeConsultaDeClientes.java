package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeConsultaDeClientes;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeConsultaDeClientes {
    private final CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes;

    public ControladorDeConsultaDeClientes(CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes) {
        this.casoDeUsoDeConsultaDeClientes = casoDeUsoDeConsultaDeClientes;
    }

    @GetMapping
    public List<ClienteDeResposta> consultar(
            @RequestParam(value = "nomeFantasia", required = false) String nomeFantasia) {
        try {
            return casoDeUsoDeConsultaDeClientes.consultar(nomeFantasia);
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(HttpStatus.BAD_REQUEST, failure.getMessage());
        }
    }
}