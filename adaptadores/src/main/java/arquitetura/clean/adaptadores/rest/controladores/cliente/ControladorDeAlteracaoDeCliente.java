package arquitetura.clean.adaptadores.rest.controladores.cliente;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.ClienteNaoEncontrado;
import arquitetura.clean.aplicacao.modelo.requisicao.cliente.ClienteDeRequisicao;
import arquitetura.clean.aplicacao.modelo.resposta.cliente.ClienteDeResposta;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.clean.adaptadores.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeAlteracaoDeCliente {
    private final CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente;

    public ControladorDeAlteracaoDeCliente(CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente) {
        this.casoDeUsoDeAlteracaoDeCliente = casoDeUsoDeAlteracaoDeCliente;
    }

    @PutMapping
    public ClienteDeResposta alterar(@RequestBody ClienteDeRequisicao clienteDeRequisicao) {
        try {
            return casoDeUsoDeAlteracaoDeCliente.alterar(clienteDeRequisicao);
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(HttpStatus.BAD_REQUEST, failure.getMessage());
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(HttpStatus.NOT_FOUND, failure.getMessage());
        }
    }
}