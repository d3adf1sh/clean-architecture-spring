package arquitetura.clean.adaptadores.rest.resposta;

import lombok.Getter;
import org.springframework.http.ResponseEntity;

public final class ErroWeb extends RuntimeException {
    @Getter
    private final ResponseEntity<RespostaWeb> resposta;

    public ErroWeb(ResponseEntity<RespostaWeb> resposta) {
        super(resposta.getBody() != null ? resposta.getBody().mensagem() : null);
        this.resposta = resposta;
    }
}