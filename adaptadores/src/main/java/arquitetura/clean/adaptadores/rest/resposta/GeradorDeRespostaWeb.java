package arquitetura.clean.adaptadores.rest.resposta;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class GeradorDeRespostaWeb {
    private GeradorDeRespostaWeb() {
    }

    public static ErroWeb gerarErro(HttpStatus status, String mensagem) {
        return new ErroWeb(gerarResposta(status, mensagem));
    }

    private static ResponseEntity<RespostaWeb> gerarResposta(HttpStatus status, String mensagem) {
        RespostaWeb respostaWeb = new RespostaWeb(status.value(), mensagem);
        return gerarResposta(status, respostaWeb);
    }

    public static <T> ResponseEntity<T> gerarResposta(HttpStatus status, T entidade) {
        return ResponseEntity.status(status)
                .body(entidade);
    }
}
