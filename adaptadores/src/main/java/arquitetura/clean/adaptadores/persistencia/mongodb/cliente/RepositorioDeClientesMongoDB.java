package arquitetura.clean.adaptadores.persistencia.mongodb.cliente;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepositorioDeClientesMongoDB extends MongoRepository<ClienteMongoDB, String> {
    @Query("{cpfj: ?0}")
    Optional<ClienteMongoDB> buscar(String cpfj);

    @Query("{nomeFantasia: {$regex: ?0}})")
    List<ClienteMongoDB> consultar(String nomeFantasia);
}