package arquitetura.clean.adaptadores.persistencia.jpa.cliente;

import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.modelo.entidades.cliente.Cliente;
import arquitetura.clean.modelo.valores.CPFJ;
import jakarta.transaction.Transactional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnExpression("'${persistencia}'.equals('mysql') || '${persistencia}'.equals('postgres')")
public class RepositorioDeClientesPadrao implements RepositorioDeClientes {
    private final RepositorioDeClientesJPA repositorioDeClientesJPA;

    public RepositorioDeClientesPadrao(RepositorioDeClientesJPA repositorioDeClientesJPA) {
        this.repositorioDeClientesJPA = repositorioDeClientesJPA;
    }

    @Override
    @Transactional
    public Cliente inserir(Cliente cliente) {
        ClienteJPA clienteJPA = ClienteJPA.converterParaJPA(cliente);
        clienteJPA.setDataDeInsercao(LocalDateTime.now());
        repositorioDeClientesJPA.save(clienteJPA);
        return ClienteJPA.converterParaModelo(clienteJPA);
    }

    @Override
    @Transactional
    public Optional<Cliente> alterar(Cliente cliente) {
        Optional<ClienteJPA> clienteJPASalvo = repositorioDeClientesJPA.buscar(cliente.getCpfj().valor());
        if (clienteJPASalvo.isPresent()) {
            ClienteJPA clienteJPA = ClienteJPA.converterParaJPA(cliente);
            clienteJPA.setIdCliente(clienteJPASalvo.get().getIdCliente());
            clienteJPA.setDataDeAlteracao(LocalDateTime.now());
            repositorioDeClientesJPA.save(clienteJPA);
            return Optional.of(ClienteJPA.converterParaModelo(clienteJPA));
        } else {
            return Optional.empty();
        }
    }

    @Override
    @Transactional
    public Optional<Cliente> excluir(CPFJ cpfj) {
        Optional<ClienteJPA> clienteJPA = repositorioDeClientesJPA.buscar(cpfj.valor());
        if (clienteJPA.isPresent()) {
            repositorioDeClientesJPA.delete(clienteJPA.get());
            return clienteJPA.map(ClienteJPA::converterParaModelo);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Cliente> buscar(CPFJ cpfj) {
        Optional<ClienteJPA> clienteJPA = repositorioDeClientesJPA.buscar(cpfj.valor());
        return clienteJPA.map(ClienteJPA::converterParaModelo);
    }

    @Override
    public List<Cliente> consultar(String nomeFantasia) {
        List<ClienteJPA> clientesJPA = repositorioDeClientesJPA.consultar("%" + nomeFantasia + "%");
        return clientesJPA.stream()
                .map(ClienteJPA::converterParaModelo)
                .toList();
    }
}
