package arquitetura.clean.adaptadores.persistencia.mongodb.cliente;

import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.modelo.entidades.cliente.Cliente;
import arquitetura.clean.modelo.valores.CPFJ;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "persistencia", havingValue = "mongodb")
public class RepositorioDeClientesPadrao implements RepositorioDeClientes {
    private final RepositorioDeClientesMongoDB repositorioDeClientesMongoDB;

    public RepositorioDeClientesPadrao(RepositorioDeClientesMongoDB repositorioDeClientesMongoDB) {
        this.repositorioDeClientesMongoDB = repositorioDeClientesMongoDB;
    }

    @Override
    public Cliente inserir(Cliente cliente) {
        ClienteMongoDB clienteMongoDB = ClienteMongoDB.converterParaMongoDB(cliente);
        clienteMongoDB.setDataDeInsercao(LocalDateTime.now());
        repositorioDeClientesMongoDB.save(clienteMongoDB);
        return ClienteMongoDB.converterParaModelo(clienteMongoDB);
    }

    @Override
    public Optional<Cliente> alterar(Cliente cliente) {
        Optional<ClienteMongoDB> clienteMongoDBSalvo = repositorioDeClientesMongoDB.buscar(cliente.getCpfj().valor());
        if (clienteMongoDBSalvo.isPresent()) {
            ClienteMongoDB clienteMongoDB = ClienteMongoDB.converterParaMongoDB(cliente);
            clienteMongoDB.setId(clienteMongoDBSalvo.get().getId());
            clienteMongoDB.setDataDeInsercao(clienteMongoDBSalvo.get().getDataDeInsercao());
            clienteMongoDB.setDataDeAlteracao(LocalDateTime.now());
            repositorioDeClientesMongoDB.save(clienteMongoDB);
            return Optional.of(ClienteMongoDB.converterParaModelo(clienteMongoDB));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Cliente> excluir(CPFJ cpfj) {
        Optional<ClienteMongoDB> clienteMongoDB = repositorioDeClientesMongoDB.buscar(cpfj.valor());
        if (clienteMongoDB.isPresent()) {
            repositorioDeClientesMongoDB.delete(clienteMongoDB.get());
            return clienteMongoDB.map(ClienteMongoDB::converterParaModelo);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Cliente> buscar(CPFJ cpfj) {
        Optional<ClienteMongoDB> clienteMongoDB = repositorioDeClientesMongoDB.buscar(cpfj.valor());
        return clienteMongoDB.map(ClienteMongoDB::converterParaModelo);
    }

    @Override
    public List<Cliente> consultar(String nomeFantasia) {
        List<ClienteMongoDB> clientesJPA = repositorioDeClientesMongoDB.consultar(nomeFantasia);
        return clientesJPA.stream()
                .map(ClienteMongoDB::converterParaModelo)
                .toList();
    }
}
