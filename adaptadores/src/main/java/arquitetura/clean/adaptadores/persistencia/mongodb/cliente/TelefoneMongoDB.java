package arquitetura.clean.adaptadores.persistencia.mongodb.cliente;

import arquitetura.clean.modelo.entidades.cliente.Telefone;
import arquitetura.clean.modelo.valores.Numero;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TelefoneMongoDB {
    private String descricao;
    private String numero;

    public static TelefoneMongoDB converterParaMongoDB(Telefone telefone) {
        TelefoneMongoDB telefoneMongoDB = new TelefoneMongoDB();
        telefoneMongoDB.setDescricao(telefone.getDescricao());
        if (telefone.getNumero() != null) {
            telefoneMongoDB.setNumero(telefone.getNumero().valor());
        }

        return telefoneMongoDB;
    }

    public static Telefone converterParaModelo(TelefoneMongoDB telefoneMongoDB) {
        Numero numero = telefoneMongoDB.getNumero() != null ? new Numero(telefoneMongoDB.getNumero()) : null;
        return new Telefone(telefoneMongoDB.getDescricao(), numero);
    }
}