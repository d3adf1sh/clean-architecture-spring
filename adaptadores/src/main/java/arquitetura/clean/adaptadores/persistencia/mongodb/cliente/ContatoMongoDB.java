package arquitetura.clean.adaptadores.persistencia.mongodb.cliente;

import arquitetura.clean.modelo.entidades.cliente.Contato;
import arquitetura.clean.modelo.entidades.cliente.Telefone;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ContatoMongoDB {
    private String nome;
    private List<TelefoneMongoDB> telefones;

    public static ContatoMongoDB converterParaMongoDB(Contato contato) {
        ContatoMongoDB contatoMongoDB = new ContatoMongoDB();
        contatoMongoDB.setNome(contato.getNome());
        if (contato.getTelefones() != null) {
            contatoMongoDB.setTelefones(
                    contato.getTelefones().stream().map(TelefoneMongoDB::converterParaMongoDB).toList());
        }

        return contatoMongoDB;
    }

    public static Contato converterParaModelo(ContatoMongoDB contatoMongoDB) {
        List<Telefone> telefones = contatoMongoDB.getTelefones() != null
                ? contatoMongoDB.getTelefones().stream().map(TelefoneMongoDB::converterParaModelo).toList() : null;
        return new Contato(contatoMongoDB.getNome(), telefones);
    }
}