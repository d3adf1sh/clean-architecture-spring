package arquitetura.clean.adaptadores;

import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeConsultaDeClientes;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.clean.aplicacao.casosdeuso.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.clean.aplicacao.repositorios.RepositorioDeClientes;
import arquitetura.clean.aplicacao.servicos.cliente.ServicoDeAlteracaoDeCliente;
import arquitetura.clean.aplicacao.servicos.cliente.ServicoDeBuscaDeCliente;
import arquitetura.clean.aplicacao.servicos.cliente.ServicoDeConsultaDeClientes;
import arquitetura.clean.aplicacao.servicos.cliente.ServicoDeExclusaoDeCliente;
import arquitetura.clean.aplicacao.servicos.cliente.ServicoDeInsercaoDeCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Adaptadores {
    @Autowired
    RepositorioDeClientes repositorioDeClientes;

    @Bean
    CasoDeUsoDeInsercaoDeCliente getCasoDeUsoDeInsercaoDeCliente() {
        return new ServicoDeInsercaoDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeAlteracaoDeCliente getCasoDeUsoDeAlteracaoDeCliente() {
        return new ServicoDeAlteracaoDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeExclusaoDeCliente getCasoDeUsoDeExclusaoDeCliente() {
        return new ServicoDeExclusaoDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeBuscaDeCliente getCasoDeUsoDeBuscaDeCliente() {
        return new ServicoDeBuscaDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeConsultaDeClientes getCasoDeUsoDeConsultaDeClientes() {
        return new ServicoDeConsultaDeClientes(repositorioDeClientes);
    }
}
